module.exports = function(app){
	// se carga el esquema desde el archivo film
	var Film = require('./films');

	//GET
	findAllFilms = function(req, res){
		Film.find(function(err, films){
			if(!err) res.send(films);
			else console.log("Error buscar todos: "+err);
		});
	};

	// POST
	addFilm = function(req, res){
       
       console.log(req.body);
		
		var film = new Film({
			title: req.body.title,
	        year: req.body.year,
			urlTrailer: req.body.urlTrailer,
			description: req.body.description
		}); 

		film.save(function(err){
			if(!err) console.log('Película salvada');
			else console.log('Error salvando la película '+err);
		});

		res.send(film);
	};

	 //GET
	findByID = function(req, res){
		Film.findById(req.params.id, function(err, film){
			if(!err) res.send(film);
			else console.log("Error buscar todos: "+err);
		});

	};

	// PUT update
	updateFilm = function(req, res){

		Film.findById(req.params.id, function(err, film){
			
			film.title = req.body.title;
			film.year = req.body.year;
			film.urlTrailer = req.body.urlTrailer;
			film.description = req.body.description;

			film.save(function(err){
				if(!err){
					console.log('Película actualizada');
					res.send(film);
				} 
				else console.log('Error actualizando la película '+err);
			});

		});

	};

	// Delete
	deleteFilm = function(req, res){
		Film.findById(req.params.id, function(err, film){
			
			film.remove(function(err){
				if(!err){
					res.send(film);
					console.log('Película eliminada');
				} 
				else console.log('Error eliminando la película '+err);
			})


		});
		
	};

	// API Routes
	app.get('/films', findAllFilms);
	app.post('/films', addFilm);
	app.get('/films/:id', findByID);
	app.put('/films/:id', updateFilm);
	app.delete('/films/:id', deleteFilm);

} 