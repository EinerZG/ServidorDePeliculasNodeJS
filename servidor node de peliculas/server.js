
// se incluyen las dependencias que se van a usar
var express = require('express')
var mongoose = require('mongoose')
var app = express();

// se configura el servicio
// permite usar los verbos REST
app.configure(function () {
  // permite que se pueda parsear el JSON
  app.use(express.bodyParser());
  // permite implementar y personlizar los metodos http
  app.use(express.methodOverride());
  // permite crear rutas personalizadas
  app.use(app.router);
});

// peticion get
app.get('/', function(req, res) {
  res.send("Hola Android!!! ");
});

// se realiza el llamado a la base de datos
mongoose.connect('mongodb://localhost/films', function(err, res){
	if(err) console.log("Error en la conexión a la BD "+err)
	else console.log("Conexión exitosa a la BD");	
});

require('./routes')(app);

// se prepara el servicio para que escuche por el puerto 3000 
app.listen(3000, function() {
  console.log("Servidor node corriendo en http://localhost:3000");
});